package org.app.mockito.ejemplo;

import org.app.mockito.ejemplo.model.Examen;

import java.util.Arrays;
import java.util.List;

public class DatosTesting {
    public static final List<Examen> EXAMENES =  Arrays.asList(new Examen(1L, "Java"), new Examen(2L, "C++"));
    public static final List<Examen> EXAMENES_ID_NEGATIVOS =  Arrays.asList(new Examen(-1L, "Java"), new Examen(null, "C++"));
    public static final List<Examen> EXAMENES_ID_NULL =  Arrays.asList(new Examen(null, "Java"), new Examen(null, "C++"));
    public static final List<String> PREGUNTAS = Arrays.asList("POO", "HERENCIA", "ENCAPSULAMIENTO", "POLIMORFISMO");
    public static final Examen EXAMEN = new Examen(3L, "Algoritmos");
}
