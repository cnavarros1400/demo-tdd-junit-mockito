package org.app.mockito.ejemplo.repository;

import org.app.mockito.ejemplo.DatosTesting;
import org.app.mockito.ejemplo.model.Examen;

import java.util.List;

public class ExamenRepositoryImp implements ExamenRepository{
    @Override
    public List<Examen> encotrarTodos() {
        System.out.println("ExamenRepositoryImp.encotrarTodos");
        return DatosTesting.EXAMENES;
    }

    @Override
    public Examen guardarExamen(Examen examen) {
        System.out.println("ExamenRepositoryImp.guardarExamen");
        return DatosTesting.EXAMEN;
    }
}
