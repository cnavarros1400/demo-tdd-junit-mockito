package org.app.mockito.ejemplo.service;

import org.app.mockito.ejemplo.model.Examen;
import org.app.mockito.ejemplo.repository.ExamenRepository;
import org.app.mockito.ejemplo.repository.PreguntaRepository;

import java.util.List;
import java.util.Optional;

public class ExamenServiceImp implements ExamenService{
    private ExamenRepository examenRepository;
    private PreguntaRepository preguntaRepository;

    public ExamenServiceImp(ExamenRepository examenRepository, PreguntaRepository preguntaRepository) {
        this.examenRepository = examenRepository;
        this.preguntaRepository = preguntaRepository;
    }

    @Override
    public Optional<Examen> buscarPorNombre(String nombreExamen) {
        return examenRepository.encotrarTodos().stream().filter(ex -> ex.getNombre().equals(nombreExamen)).findFirst();
    }

    @Override
    public Examen buscarPorNombreConPreguntas(String nombre) {
        Optional<Examen> examenOptional = buscarPorNombre(nombre);
        Examen examen = null;
        if (examenOptional.isPresent()){
            examen = examenOptional.get();
            //examen = examenOptional.orElseThrow();//opcional a .get()
            List<String> preguntas = preguntaRepository.encontrarPorExamenId(examen.getId());
            preguntaRepository.encontrarPorExamenId(examen.getId());//para testeo numero de invocaciones
            examen.setPreguntas(preguntas);
        }
        return examen;
    }

    @Override
    public Examen guardar(Examen examen) {
        if (!examen.getPreguntas().isEmpty()){
            preguntaRepository.guardarPreguntas(examen.getPreguntas());
        }
        return examenRepository.guardarExamen(examen);
    }
}
