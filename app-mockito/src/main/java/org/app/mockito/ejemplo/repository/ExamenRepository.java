package org.app.mockito.ejemplo.repository;

import org.app.mockito.ejemplo.model.Examen;

import java.util.List;

public interface ExamenRepository {
    List<Examen> encotrarTodos();
    Examen guardarExamen(Examen examen);

}
