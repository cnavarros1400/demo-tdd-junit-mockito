package org.app.mockito.ejemplo.repository;

import org.app.mockito.ejemplo.DatosTesting;

import java.util.List;

public class PreguntaRepositoryImp implements PreguntaRepository{
    @Override
    public List<String> encontrarPorExamenId(Long id) {
        System.out.println("PreguntaRepositoryImp.encontrarPorExamenId");
        return DatosTesting.PREGUNTAS;
    }

    @Override
    public void guardarPreguntas(List<String> preguntas) {
        System.out.println("PreguntaRepositoryImp.guardarPreguntas");
    }
}
