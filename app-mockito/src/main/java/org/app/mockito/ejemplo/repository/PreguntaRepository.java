package org.app.mockito.ejemplo.repository;

import java.util.List;

public interface PreguntaRepository {
    List<String> encontrarPorExamenId(Long id);
    void guardarPreguntas(List<String> preguntas);
}
