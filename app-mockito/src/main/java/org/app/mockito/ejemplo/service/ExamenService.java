package org.app.mockito.ejemplo.service;

import org.app.mockito.ejemplo.model.Examen;

import java.util.Optional;

public interface ExamenService {
    Optional<Examen> buscarPorNombre(String nombreExamen);
    Examen buscarPorNombreConPreguntas(String nombre);
    Examen guardar(Examen examen);

}
