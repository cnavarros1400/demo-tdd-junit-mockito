package org.app.mockito.ejemplo.service;

import org.app.mockito.ejemplo.DatosTesting;
import org.app.mockito.ejemplo.model.Examen;
import org.app.mockito.ejemplo.repository.ExamenRepository;
import org.app.mockito.ejemplo.repository.ExamenRepositoryImp;
import org.app.mockito.ejemplo.repository.PreguntaRepository;
import org.app.mockito.ejemplo.repository.PreguntaRepositoryImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)//habilita anotaciones para injeccion de dependencias
class ExamenServiceImpTest {

    @Captor ArgumentCaptor<Long> captor;
    @Mock
    ExamenRepositoryImp examenRepository;
    @Mock
    PreguntaRepositoryImp preguntaRepository;
    @InjectMocks ExamenServiceImp examenService;//injecta en el constructor los @Mock
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);//habilitando el uso de anotaciones
    }

    @Test
    void buscarPorNombreTest() {
        //ExamenRepository examenRepository = new ExamenRepositoryImp();
        //ExamenRepository examenRepository = Mockito.mock(ExamenRepository.class);//clase o interfaz a simular

        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        Optional<Examen> examen = examenService.buscarPorNombre("C++");

        assertTrue(examen.isPresent());
        assertEquals(2L, examen.orElseThrow().getId());
        assertEquals("C++", examen.get().getNombre());
    }
    @Test
    void buscarPorNombreListaVaciaTest() {
        List<Examen> resultados = Collections.emptyList();
        when(examenRepository.encotrarTodos()).thenReturn(resultados);
        Optional<Examen> examen = examenService.buscarPorNombre("C++");
        assertFalse(examen.isPresent());
    }

    @Test
    void testPreguntasExamen(){
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        when(preguntaRepository.encontrarPorExamenId(1L)).thenReturn(DatosTesting.PREGUNTAS);
        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(4, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("POO"));
    }

    @Test
    void testPreguntasExamenVerify(){
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        when(preguntaRepository.encontrarPorExamenId(1L)).thenReturn(DatosTesting.PREGUNTAS);
        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(4, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("POO"));
        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(1L);
    }

    @Test
    void testNoExisteExamenVerify(){
        when(examenRepository.encotrarTodos()).thenReturn(Collections.emptyList());
        when(preguntaRepository.encontrarPorExamenId(1L)).thenReturn(DatosTesting.PREGUNTAS);
        Examen examen = examenService.buscarPorNombreConPreguntas("Javagdfgdg");
        assertNull(examen);
        //haciendo falllar el test ya qu eno se invocan metodos
        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(1L);
    }

    @Test
    void guardarExamen(){//modificando el valor de entrada para devolverlo en when
        Examen examenPreg = DatosTesting.EXAMEN;
        examenPreg.setPreguntas(DatosTesting.PREGUNTAS);
        when(examenRepository.guardarExamen(any(Examen.class))).then(new Answer<Examen>(){
            Long secuencia = 2L;
            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(++secuencia);
                return examen;
            }
        });
        Examen examen = examenService.guardar(examenPreg);
        assertNotNull(examen.getId());
        assertEquals(3L, examen.getId());
        assertEquals("Algoritmos", examen.getNombre());
        verify(examenRepository).guardarExamen(any(Examen.class));
        verify(preguntaRepository).guardarPreguntas(anyList());
    }

    @Test
    void testManejoExcepciones() {
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES_ID_NULL);
        when(preguntaRepository.encontrarPorExamenId(isNull())).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> {
            examenService.buscarPorNombreConPreguntas("Java");
        });
        //confirmando lo anterior
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            examenService.buscarPorNombreConPreguntas("Java");
        });
        assertEquals(IllegalArgumentException.class, exception.getClass());

        verify(examenRepository, times(2)).encotrarTodos();
        verify(preguntaRepository, times(2)).encontrarPorExamenId(isNull());
    }

    @Test
    void argumentMatchers() {//argument matcher: asegurando que conciden argumentos "recuperados" de los mocks
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(DatosTesting.PREGUNTAS);
        examenService.buscarPorNombreConPreguntas("Java");

        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(argThat(arg -> arg.equals(1L)));
        //Otras opciones
        verify(preguntaRepository).encontrarPorExamenId(eq(1L));
        verify(preguntaRepository).encontrarPorExamenId(argThat(arg -> arg.equals(1L) && arg >= 1L && arg != null));
    }

    @Test
    void CustomArgumentMatchers() {
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES_ID_NEGATIVOS);
        when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(DatosTesting.PREGUNTAS);
        examenService.buscarPorNombreConPreguntas("Java");

        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(argThat(new CustomArgstMatcher()));
    }

    @Test
    void CustomArgumentMatchersLambda() {//usando exp lambda, sin mensaje custom de error
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES_ID_NEGATIVOS);
        when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(DatosTesting.PREGUNTAS);
        examenService.buscarPorNombreConPreguntas("Java");

        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(argThat((argument) -> argument != null && argument >= 1));
    }

    public static class CustomArgstMatcher implements ArgumentMatcher<Long>{//clase anidada, puede ser externa
        private Long arg;
        @Override
        public boolean matches(Long argument) {
            this.arg = argument;
            return argument != null && argument >= 1;
        }

        @Override
        public String toString() {
            return "Mensaje custom de error arrojado por mockito en caso de fallar test: Error: Argumento '" + arg.toString() +"' debe ser entero positivo";
        }
    }

    @Test
    void testArgumentCaptor() {
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(DatosTesting.PREGUNTAS);
        examenService.buscarPorNombreConPreguntas("Java");
        //ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);//reempleazable por anotaciones
        verify(preguntaRepository).encontrarPorExamenId(captor.capture());
        assertEquals(1L, captor.getValue());
    }

    @Test
    void testDoThrow() {//doThrow para metodos void
        Examen examen = DatosTesting.EXAMEN;
        examen.setPreguntas(DatosTesting.PREGUNTAS);
        doThrow(IllegalArgumentException.class).when(preguntaRepository).guardarPreguntas(anyList());
        assertThrows(IllegalArgumentException.class, () -> {
            examenService.guardar(examen);
        });
    }

    @Test
    void testDoAnswer() {//ejecutar evento segun un parametro pasado a repository
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        doAnswer(invocationOnMock -> {
            Long idEx = invocationOnMock.getArgument(0);
            return idEx == 1L ? DatosTesting.PREGUNTAS : null;
        }).when(preguntaRepository).encontrarPorExamenId(anyLong());
        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(1L, examen.getId());
        assertEquals("Java", examen.getNombre());
        assertEquals(4, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("POLIMORFISMO"));
    }

    @Test
    void guardarExamenDoAnswer(){//ejecutar evento segun un parametro pasado a repository
        Examen examenPreg = DatosTesting.EXAMEN;
        examenPreg.setPreguntas(DatosTesting.PREGUNTAS);
        doAnswer(new Answer<Examen>(){
                Long secuencia = 2L;
                @Override
                public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                    Examen examen = invocationOnMock.getArgument(0);
                    examen.setId(++secuencia);
                    return examen;
                }
        }).when(examenRepository).guardarExamen(any(Examen.class));
        Examen examen = examenService.guardar(examenPreg);
        assertNotNull(examen.getId());
        assertEquals(3L, examen.getId());
        assertEquals("Algoritmos", examen.getNombre());
        verify(examenRepository).guardarExamen(any(Examen.class));
        verify(preguntaRepository).guardarPreguntas(anyList());
    }

    @Test
    void testDoCallRealMethod() {
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        //when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(DatosTesting.PREGUNTAS);
        doCallRealMethod().when(preguntaRepository).encontrarPorExamenId(anyLong());
        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(1L, examen.getId());
        assertEquals("Java", examen.getNombre());
    }

    //Spy con DoReturn
    //no son mock al 100%, combinacion de mock y llamado a una copia del metodo real con caracteristicas de mock
    @Test
    void testSpyDoReturn() {
        ExamenRepository examenRepository = spy(ExamenRepositoryImp.class);//spy requiere clase implementada, no interfaz ya que usara metodos "reales"
        PreguntaRepository preguntaRepository = spy(PreguntaRepositoryImp.class);
        ExamenService examenService = new ExamenServiceImp(examenRepository, preguntaRepository);

        List<String> pregs = List.of("POLIMORFISMO");
        //when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(pregs);//evitando llamado a metodo mock
        doReturn(pregs).when(preguntaRepository).encontrarPorExamenId(anyLong());//devuelve implementacion falsa de pregunras

        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(1L, examen.getId());
        assertEquals("Java", examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("POLIMORFISMO"));

        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(anyLong());
    }

    @Test
    void testOrdenInvocaciones() {//orden de invocacion en metodos
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);

        examenService.buscarPorNombreConPreguntas("C++");
        examenService.buscarPorNombreConPreguntas("Java");

        InOrder inOrder = inOrder(preguntaRepository);
        inOrder.verify(preguntaRepository).encontrarPorExamenId(2L);
        inOrder.verify(preguntaRepository).encontrarPorExamenId(1L);
    }

    @Test
    void testOrdenInvocacionesMultiplesMocks() {//orden de invocacion en metodos
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        examenService.buscarPorNombreConPreguntas("C++");
        examenService.buscarPorNombreConPreguntas("Java");
        //Orden a verificar:
        /*
        * examenRepo
        * pregintaRepo
        * examenRepo
        * pregintaRepo
        * */
        //inicia verificacion en el siguiente orden:
        InOrder inOrder = inOrder(examenRepository, preguntaRepository);
        inOrder.verify(examenRepository).encotrarTodos();
        inOrder.verify(preguntaRepository).encontrarPorExamenId(2L);
        inOrder.verify(examenRepository).encotrarTodos();
        inOrder.verify(preguntaRepository).encontrarPorExamenId(1L);
        //finaliza verificacion del orden
    }

    @Test
    void testNumeroInvocaciones() {
        when(examenRepository.encotrarTodos()).thenReturn(DatosTesting.EXAMENES);
        examenService.buscarPorNombreConPreguntas("Java");

        verify(preguntaRepository, times(2)).encontrarPorExamenId(1L);
        verify(preguntaRepository, atLeast(1)).encontrarPorExamenId(1L);
        verify(preguntaRepository, atLeastOnce()).encontrarPorExamenId(1L);
        verify(preguntaRepository, atMost(2)).encontrarPorExamenId(1L);
        //verify(preguntaRepository, atMostOnce()).encontrarPorExamenId(1L);
    }

    @Test
    void testNumeroInvocaciones2() {
        when(examenRepository.encotrarTodos()).thenReturn(Collections.emptyList());
        examenService.buscarPorNombreConPreguntas("Java");
        verify(preguntaRepository, never()).encontrarPorExamenId(anyLong());
        verifyNoInteractions(preguntaRepository);
        verify(examenRepository).encotrarTodos();
        verify(examenRepository, times(1)).encotrarTodos();
        verify(examenRepository, atLeastOnce()).encotrarTodos();
        verify(examenRepository, atMostOnce()).encotrarTodos();

    }
}