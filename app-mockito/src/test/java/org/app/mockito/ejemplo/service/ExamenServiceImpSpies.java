package org.app.mockito.ejemplo.service;

import org.app.mockito.ejemplo.DatosTesting;
import org.app.mockito.ejemplo.model.Examen;
import org.app.mockito.ejemplo.repository.ExamenRepository;
import org.app.mockito.ejemplo.repository.ExamenRepositoryImp;
import org.app.mockito.ejemplo.repository.PreguntaRepository;
import org.app.mockito.ejemplo.repository.PreguntaRepositoryImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)//habilita anotaciones para injeccion de dependencias
class ExamenServiceImpSpies {

    @Captor ArgumentCaptor<Long> captor;
    @Spy
    ExamenRepositoryImp examenRepository;
    @Spy
    PreguntaRepositoryImp preguntaRepository;
    @InjectMocks ExamenServiceImp examenService;//injecta en el constructor los @Mock o @Spy

    //Spy con DoReturn
    //no son mock al 100%, combinacion de mock y llamado a una copia del metodo real con caracteristicas de mock
    @Test
    void testSpyDoReturn() {
        System.out.println("Testing spies con anotaciones");
        List<String> pregs = List.of("POLIMORFISMO");
        //when(preguntaRepository.encontrarPorExamenId(anyLong())).thenReturn(pregs);//evitando llamado a metodo mock
        doReturn(pregs).when(preguntaRepository).encontrarPorExamenId(anyLong());//devuelve implementacion falsa de pregunras

        Examen examen = examenService.buscarPorNombreConPreguntas("Java");
        assertEquals(1L, examen.getId());
        assertEquals("Java", examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("POLIMORFISMO"));

        verify(examenRepository).encotrarTodos();
        verify(preguntaRepository).encontrarPorExamenId(anyLong());
    }
}