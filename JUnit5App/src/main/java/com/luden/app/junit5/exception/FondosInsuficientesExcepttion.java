package com.luden.app.junit5.exception;

public class FondosInsuficientesExcepttion extends RuntimeException{
    public FondosInsuficientesExcepttion(String message) {
        super(message);
    }
}
