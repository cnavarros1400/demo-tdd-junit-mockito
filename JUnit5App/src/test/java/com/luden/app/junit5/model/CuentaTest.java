package com.luden.app.junit5.model;

import com.luden.app.junit5.exception.FondosInsuficientesExcepttion;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class CuentaTest {

    Cuenta cuenta;

    private TestInfo testInfo;
    private TestReporter testReporter;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Inicializando Clase CuentaTest (clase, no instancia)");

    }

    @AfterAll
    static void afterAll() {
        System.out.println("Finalizando Clase CuentaTest (clase, no instancia)");
    }

    @BeforeEach
    void initMetodoTest(TestInfo testInfo, TestReporter testReporter){
        this.testInfo = testInfo;
        this.testReporter = testReporter;
        System.out.println("Metodo: ".concat(String.valueOf(testInfo.getTestMethod().get())));
        System.out.println("Display name: ".concat(testInfo.getDisplayName()));
        System.out.println("Test method: ".concat(String.valueOf(testInfo.getTestMethod())));
        System.out.println("Etiquetas: ".concat(testInfo.getTags().toString()));
        System.out.println("Test metodo iniciado");
        this.cuenta = new Cuenta("Svein", new BigDecimal("1000.50"));
    }

    @AfterEach
    void afterEach() {
        System.out.println("Test metodo finalizado");
    }

    @Test
    @DisplayName("Test titular de la cuenta")
    @Disabled
    void test_nombre_cuenta() {
        //fail();
        System.out.println("");
        Cuenta cuenta = new Cuenta();
        cuenta.setPersona("Svein");
        String valEsperado = "Svein";
        String valReal = cuenta.getPersona();
        //Assertions.assertEquals(valEsperado, valReal);
        assertNotNull(valReal, () -> "Titular de cuenta no puede ser nulo");//usando lambda el string solo se instancia si falla el test
        assertEquals(valEsperado, valReal);
        assertTrue(valReal.equals("Svein"));
    }

    @Test
    @DisplayName("Test Saldo de la cuenta")
    void test_saldo_cuenta() {
        cuenta = new Cuenta("Svein", new BigDecimal("1000.123456"));
        assertEquals(1000.123456, cuenta.getSaldo().doubleValue());
        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        assertTrue(cuenta.getSaldo().doubleValue() > 0);
    }

    @Test
    @DisplayName("Test comparacion de objetos cuenta")
    void testReferenciaCuentaPorReferencia() {
        Cuenta cuenta = new Cuenta("John Doe", new BigDecimal("8900.50"));
        Cuenta cuenta2 = new Cuenta("John Doe", new BigDecimal("8900.50"));
        //assertNotEquals(cuenta, cuenta2);//Equals compara referencias de objetos
        assertEquals(cuenta, cuenta2);
    }

    @Test
    @DisplayName("Test debito de una cuenta")
    void testDebitoCuenta() {
        cuenta =new Cuenta("Svein Navarro", new BigDecimal("1000.50"));
        cuenta.debito(new BigDecimal("100.00"));
        assertNotNull(cuenta.getSaldo());
        assertEquals(900, cuenta.getSaldo().intValue());
        assertEquals("900.50", cuenta.getSaldo().toPlainString());
    }

    @Test
    @Tag("CreditoCuentaaaa")
    void testCreditoCuenta() {
        testReporter.publishEntry("Tags: ".concat(String.valueOf(testInfo.getTags())));
        if (!testInfo.getTags().contains("CreditoCuenta")){
            testReporter.publishEntry("No es tag creditoCuenta");
        }
        else{
            testReporter.publishEntry("Creditcuenta tag");
        }
        cuenta.credito(new BigDecimal("100"));
        assertNotNull(cuenta.getSaldo());
        assertEquals(1100, cuenta.getSaldo().intValue());
        assertEquals("1100.50", cuenta.getSaldo().toPlainString());
    }

    @Test
    void testFondosInsuficientesException() {
        this.cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000.00"));
        Exception ex = assertThrows(FondosInsuficientesExcepttion.class, () -> cuenta.debito(new BigDecimal("1000.000001")));
        String actual = ex.getMessage();
        String esperado = "Fondos insuficientes";
        assertEquals(esperado, actual);
    }

    @Test
    void testTransferir() {
        Cuenta cuenta1 = new Cuenta("Carlos Navarro", new BigDecimal("1000.00"));
        Cuenta cuenta2 = new Cuenta("Svein Segura", new BigDecimal("1000.00"));
        Banco banco = new Banco();
        banco.setNombre("papaBank");
        banco.trasferir(cuenta1, cuenta2, new BigDecimal("200.00"));
        assertEquals("800.00", cuenta1.getSaldo().toPlainString());
        assertEquals("1200.00", cuenta2.getSaldo().toPlainString());
    }

    @Test
    void testRelacionBancoCuentas() {
        Cuenta cuenta1 = new Cuenta("Carlos Navarro", new BigDecimal("1000.00"));
        Cuenta cuenta2 = new Cuenta("Svein Segura", new BigDecimal("1000.00"));
        Banco banco = new Banco();
        banco.agregarCuenta(cuenta1);
        banco.agregarCuenta(cuenta2);
        banco.setNombre("papaBank");
        banco.trasferir(cuenta1, cuenta2, new BigDecimal("200.00"));
        assertAll(() -> assertEquals("800.00", cuenta1.getSaldo().toPlainString()),
                () -> assertEquals("1200.00", cuenta2.getSaldo().toPlainString()),
                () -> assertEquals(2, banco.getCuentas().size()),
                () -> assertEquals("papaBank", cuenta1.getBanco().getNombre()),
                () -> assertEquals("papaBank", cuenta2.getBanco().getNombre()),
                () -> assertTrue(banco.getCuentas().stream()
                        .filter(cta -> cta.getPersona().equals("Carlos Navarro"))
                        .findFirst()
                        .isPresent()),
                () -> assertEquals("Carlos Navarro", banco.getCuentas().stream()
                        .filter(cta -> cta.getPersona().equals("Carlos Navarro"))
                        .findFirst()
                        .get()
                        .getPersona()),
                () -> assertTrue(banco.getCuentas().stream()
                        .anyMatch(cta -> cta.getPersona().equals("Carlos Navarro"))),
                () -> assertTrue(banco.getCuentas().stream()
                        .anyMatch(cta -> cta.getPersona().equals("Carlos Navarro"))),
                () -> assertTrue(banco.getCuentas().stream()
                        .anyMatch(cta -> cta.getPersona().equals("Svein Segura"))));
    }

    //@Test
    @RepeatedTest(value = 5, name = "{displayName} Repetición numero {currentRepetition} de {totalRepetitions}")
    @DisplayName("Test debito de una cuenta repeticiones")
    void testDebitoCuentaRepeticion(RepetitionInfo info) {
        if (info.getCurrentRepetition() == 3) {
            System.out.println("Ejecutando repeticion numero ".concat(String.valueOf(info.getCurrentRepetition())));
        }
        cuenta =new Cuenta("Svein Navarro", new BigDecimal("1000.50"));
        cuenta.debito(new BigDecimal("100.00"));
        assertNotNull(cuenta.getSaldo());
        assertEquals(900, cuenta.getSaldo().intValue());
        assertEquals("900.50", cuenta.getSaldo().toPlainString());
    }

    @Nested//NOTA NESTED CLASSES. Falla clase anidaddao test anidado, falla resto de la jerarquia
    @DisplayName("Testing sistema operativo")
    class SistemaOperativoTest{

        @Test
        @EnabledOnOs(OS.WINDOWS)
        void testSoloEnWindows(){
            System.out.println("Test ejecutable unicamente en windows");
        }

        @Test
        @EnabledOnOs({OS.LINUX, OS.MAC})
        void testSoloEnLinuxMac(){
            System.out.println("Test ejecutable unicamente en linux y mac");
        }

        @Test
        @DisabledOnOs({OS.WINDOWS})
        void noWindows(){
            System.out.println("Test deshabilitado para windows");
        }

    }

    @Nested
    @DisplayName("Testing Java version")
    class JavaVersionTest{

        @Test
        @EnabledOnJre(JRE.JAVA_8)
        void soloJavaOcho(){
            System.out.println("Test solo en java 8");
        }

        @Test
        @EnabledOnJre(JRE.JAVA_11)
        void soloJavaOnce(){
            System.out.println("Test solo en java 11");
        }

        @Test
        @EnabledOnJre(JRE.JAVA_17)
        void noEnableJava17(){
            System.out.println("Test deshabilitado en java 17");
        }

    }

    @Nested
    @DisplayName("Testing sistem properties")
    class SystemPropertiesTest{

        @Test
        void imprimirSystemProperties(){
            Properties properties =  System.getProperties();
            properties.forEach((key, val) -> System.out.println(key + " : " + val));
        }

        @Test
        @EnabledIfSystemProperty(named = "java.version", matches = "11.0.13")//matches acepta como valor expresiones regulares
        void testJavaVersionPropertySystem() {
            System.out.println("Test para la version 11, condicionando por variable de sistema");
        }

        @Test
        @DisabledIfSystemProperty(named = "os.arch", matches = ".*64.*")
        void solox32() {
            System.out.println("Deshabilitado en sistemas x64");
        }

        @Test
        @EnabledIfSystemProperty(named = "env", matches = "dev")
        void testDesarrollo() {
            System.out.println("Custom variable test");
        }

    }

    @Nested
    @DisplayName("Testing variables de entorno")
    class VariablesAmbiente{

        @Test
        void imprimirVariablesEntorno() {
            Map<String, String> vars = System.getenv();
            vars.forEach((k,v) -> System.out.println(k + " : " + v));
        }

        @Test
        @EnabledIfEnvironmentVariable(named = "JAVA_HOME", matches = ".*jdk-17")
        void testJavaHome() {
            System.out.println("Test variables de entorno");
        }

        @Test
        @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches = "16")
        void testProcesadores() {
            System.out.println("Test number of processors var entorno");
        }

        @Test
        @DisplayName("Test Saldo de la cuenta, varEntorno")
        void test_saldo_cuentaDev() {
            boolean esDev = "dev".equals(System.getProperty("env"));
            assumeTrue(esDev);
            cuenta = new Cuenta("Svein", new BigDecimal("1000.123456"));
            assertEquals(1000.123456, cuenta.getSaldo().doubleValue());
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
            assertTrue(cuenta.getSaldo().doubleValue() > 0);
        }

        @Test
        @DisplayName("Test Saldo de la cuenta, varEntorno 2")
        void test_saldo_cuentaDev2() {
            System.out.println("Haciendo 'bloques' de pruebas");
            boolean esDev = "dev".equals(System.getProperty("env"));
            assumingThat(esDev, () -> {
                cuenta = new Cuenta("Svein", new BigDecimal("1000.123456"));
                assertEquals(1000.123456, cuenta.getSaldo().doubleValue());
                assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
                assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
                assertTrue(cuenta.getSaldo().doubleValue() > 0);
            });
        }
    }


    @Tag("testingParamxxxxxxxxxxxxx")//tag aplicado a todos los testing de este nested class
    @Nested
    @DisplayName("Testing pruebas parametrizadas")
    class testsParametrizados{

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @ValueSource(strings = {"100", "200", "300", "1000"})//pueden ser ints/doubles. Strins es mas preciso con BigDecimal
        @DisplayName("Test debito de una cuenta - parametros")
        void testDebitoCuentaParametros(String monto) {
            cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000"));
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvSource({"1, 100", "2, 200", "3, 300", "4, 1000", "5, 1000.50"})//indice y valor
        @DisplayName("Test debito de una cuenta - parametros CSV source")
        void testDebitoCuentaParametrosCsvSource(String indice, String monto) {
            System.out.println("Index: " + indice + " -> " + monto);
            cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000"));
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvFileSource(resources = "/data.csv")//archivo debe estar dentro de resources
        @DisplayName("Test debito de una cuenta - parametros CSV file")
        void testDebitoCuentaParametrosCsvFile(String monto) {
            System.out.println("Parametro de monto: " + monto);
            cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000"));
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @MethodSource("listadoMontos")//nombre del metodo que devuelve lista de montos
        @DisplayName("Test debito de una cuenta - parametros method source")
        void testDebitoCuentaMethodSource(String monto) {
            System.out.println("Parametro de monto: " + monto);
            cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000"));
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        List<String> listadoMontos(){
            return Arrays.asList("100", "200", "300", "1000", "1000.50");
        }

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvSource({"200, 100, Svein, Svein", "199, 200, Svein, Svein", "500, 300, Carlos, Svein", "1000, 1000, Carlos, Carlos"})//indice y valor
        @DisplayName("Test debito de una cuenta - parametros CSV source 2")
        void testDebitoCuentaParametrosCsvSource2(String saldo, String monto, String esperado, String actual) {
            System.out.println("Saldo: " + saldo + " -> " + monto);
            cuenta = new Cuenta("Svein Navarro", new BigDecimal(saldo));
            cuenta.setSaldo(new BigDecimal(saldo));
            cuenta.debito(new BigDecimal(monto));
            cuenta.setPersona(actual);
            assertNotNull(cuenta.getPersona());
            assertNotNull(cuenta.getSaldo());
            assertEquals(esperado, actual);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "numero: {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvFileSource(resources = "/data2.csv")//archivo debe estar dentro de resources
        @DisplayName("Test debito de una cuenta - parametros CSV file 2")
        void testDebitoCuentaParametrosCsvFile2(String saldo, String monto, String esperado, String actual) {
            System.out.println("Parametro de monto: " + monto);
            cuenta = new Cuenta("Svein Navarro", new BigDecimal("1000"));
            cuenta.setSaldo(new BigDecimal(saldo));
            cuenta.debito(new BigDecimal(monto));
            cuenta.setPersona(actual);
            assertNotNull(cuenta.getPersona());
            assertNotNull(cuenta.getSaldo());
            assertEquals(esperado, actual);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

    }
    
    @Tag("timeout")
    @DisplayName("Testing timeout")
    @Nested
    class Timeouts {
        @Test
        @Timeout(5)
        void pruebaTimeOut() {
            try {
                TimeUnit.SECONDS.sleep(6);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        @Test
        @Timeout(value = 1000, unit = TimeUnit.MILLISECONDS)
        void pruebaTimeOut2() throws InterruptedException {
            TimeUnit.SECONDS.sleep(1);
        }

        @Test

        void pruebaTimeoutAssertion(){
            assertTimeout(Duration.ofSeconds(5), () -> {
                TimeUnit.SECONDS.sleep(6);
            });
        }

    }

    
}