package com.luden.app.demotestingspringboot;

import com.luden.app.demotestingspringboot.model.Banco;
import com.luden.app.demotestingspringboot.model.Cuenta;

import java.math.BigDecimal;
import java.util.Optional;

public class Datos {
//    public static final Cuenta CTA1 = new Cuenta(1L, "Carlos", new BigDecimal("1000"));
//    public static final Cuenta CTA2 = new Cuenta(2L, "Svein", new BigDecimal("2000"));
//    public static final Banco BANCO = new Banco(1L, "PapaBanco",0);

    public static Optional<Cuenta> crearCuenta001(){
        return Optional.of(new Cuenta(1L, "Carlos", new BigDecimal("1000")));
    }
    public static Optional<Cuenta> crearCuenta002(){
        return Optional.of(new Cuenta(2L, "Svein", new BigDecimal("2000")));
    }

    public static Optional<Banco> crearBanco(){
        return Optional.of(new Banco(1L, "PapaBanco",0));
    }

}
