package com.luden.app.demotestingspringboot.controller;

import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.model.TransaccionDto;
import com.luden.app.demotestingspringboot.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/cuenta")
public class CuentaController {
    @Autowired
    private CuentaService cuentaService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Cuenta> listarCuentas(){
        return cuentaService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cuenta guardar(@RequestBody Cuenta cuenta){
        return cuentaService.save(cuenta);
    }


    @GetMapping("/{idCuenta}")
    public ResponseEntity<?> detalle(@PathVariable Long idCuenta){
        Cuenta cuenta = null;
        try {
            cuenta = cuentaService.findCuentaById(idCuenta);
        }catch (NoSuchElementException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cuenta);
    }

    @PostMapping("/transferir")
    public ResponseEntity<?> transferir(@RequestBody TransaccionDto dto){
        cuentaService.transferir(dto.getBancoId(), dto.getCuentaOrigen(), dto.getCuentaDestino(), dto.getMonto());
        Map<String, Object> resp = new HashMap();
        resp.put("fecha", LocalDate.now().toString());
        resp.put("status", "ok");
        resp.put("mensaje", "transferencia realizada con exito");
        resp.put("detalle", dto);
        return ResponseEntity.ok(resp);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminar(@PathVariable Long id){
        cuentaService.deleteById(id);
    }

}
