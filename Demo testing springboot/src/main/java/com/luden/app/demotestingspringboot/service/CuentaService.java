package com.luden.app.demotestingspringboot.service;

import com.luden.app.demotestingspringboot.model.Cuenta;

import java.math.BigDecimal;
import java.util.List;

public interface CuentaService{
    List<Cuenta> findAll();
    Cuenta findCuentaById(Long idCuenta);
    Cuenta save (Cuenta cuenta);
    int consultarTotalTransferencias(Long idBanco);
    BigDecimal consultarSaldo(Long idCuenta);
    void transferir(Long idBanco, Long cuentaOrigen, Long cuentaDestino, BigDecimal monto);
    void deleteById(Long idCuenta);
}
