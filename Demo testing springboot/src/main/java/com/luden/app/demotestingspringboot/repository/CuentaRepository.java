package com.luden.app.demotestingspringboot.repository;

import com.luden.app.demotestingspringboot.model.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CuentaRepository extends JpaRepository<Cuenta, Long> {
    //List<Cuenta> findAll();
    //Cuenta findCuenteById(Long idCuenta);
    //void updateCuenta(Cuenta cuenta);
    Optional<Cuenta> findByTitular(String titular);
    @Query("select c from Cuenta c where c.titular=?1")
    Optional<Cuenta> findByTitularQuery(String titular);
}
