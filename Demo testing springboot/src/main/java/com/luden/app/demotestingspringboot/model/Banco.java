package com.luden.app.demotestingspringboot.model;

import javax.persistence.*;

@Entity
@Table(name = "banco")
public class Banco {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBanco;
    @Column(name = "nombre_banco")
    private String nombreBanco;
    @Column(name = "total_transferencias")
    private int totalTransferencias;

    public Banco() {
    }

    public Banco(Long idBanco, String nombreBanco, int totalTransferencias) {
        this.idBanco = idBanco;
        this.nombreBanco = nombreBanco;
        this.totalTransferencias = totalTransferencias;
    }

    public Long getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Long idBanco) {
        this.idBanco = idBanco;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public int getTotalTransferencias() {
        return totalTransferencias;
    }

    public void setTotalTransferencias(int totalTransferencias) {
        this.totalTransferencias = totalTransferencias;
    }
}
