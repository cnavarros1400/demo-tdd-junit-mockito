package com.luden.app.demotestingspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTestingSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTestingSpringbootApplication.class, args);
	}

}
