package com.luden.app.demotestingspringboot.service;

import com.luden.app.demotestingspringboot.model.Banco;
import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.repository.BancoRepository;
import com.luden.app.demotestingspringboot.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service //anotando para registrar bean en el uso de testing integracion de spring, y tambien para controller con autowired
public class CuentaServiceImp implements CuentaService{
    private CuentaRepository cuentaRepository;
    private BancoRepository bancoRepository;

    public CuentaServiceImp(CuentaRepository cuentaRepository, BancoRepository bancoRepository) {
        this.cuentaRepository = cuentaRepository;
        this.bancoRepository = bancoRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> findAll() {
        return cuentaRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Cuenta findCuentaById(Long idCuenta) {
        return cuentaRepository.findById(idCuenta).orElseThrow();
    }

    @Override
    @Transactional
    public Cuenta save(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

    @Override
    @Transactional(readOnly = true)
    public int consultarTotalTransferencias(Long idBanco) {
        Banco banco = bancoRepository.findById(idBanco).orElseThrow();
        return banco.getTotalTransferencias();
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal consultarSaldo(Long idCuenta) {
        Cuenta cuenta = cuentaRepository.findById(idCuenta).orElseThrow();
        return cuenta.getSaldo();
    }

    @Override
    @Transactional
    public void transferir(Long bancoId, Long cuentaOrigen, Long cuentaDestino, BigDecimal monto) {
        //debito a cuenta origen
        Cuenta ctaOrigen = cuentaRepository.findById(cuentaOrigen).orElseThrow();
        ctaOrigen.debito(monto);
        cuentaRepository.save(ctaOrigen);
        //credito cuenta destino
        Cuenta ctaDestino = cuentaRepository.findById(cuentaDestino).orElseThrow();
        ctaDestino.credito(monto);
        cuentaRepository.save(ctaDestino);
        //modificacion total transacciones
        Banco banco = bancoRepository.findById(bancoId).orElseThrow();
        int totalTransferencias = banco.getTotalTransferencias();
        banco.setTotalTransferencias(++totalTransferencias);
        bancoRepository.save(banco);
    }

    @Override
    @Transactional
    public void deleteById(Long idCuenta) {
        cuentaRepository.deleteById(idCuenta);
    }
}
