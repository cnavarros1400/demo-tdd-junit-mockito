package com.luden.app.demotestingspringboot.repository;

import com.luden.app.demotestingspringboot.model.Banco;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BancoRepository extends JpaRepository<Banco, Long> {
//    Banco findBancoById(Long idBanco);
//    List<Banco> findAllBancos();
//    void updateBanco(Banco banco);

}
