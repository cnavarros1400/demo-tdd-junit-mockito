package com.luden.app.demotestingspringboot;

import com.luden.app.demotestingspringboot.exception.SaldoInsuficienteException;
import com.luden.app.demotestingspringboot.model.Banco;
import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.repository.BancoRepository;
import com.luden.app.demotestingspringboot.repository.CuentaRepository;
import com.luden.app.demotestingspringboot.service.CuentaServiceImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static com.luden.app.demotestingspringboot.Datos.*;

@SpringBootTest
@Tag("servicio")
class TestingServicio {
	//MockBean //implementacion de Spring
	//@MockBean
	@Mock CuentaRepository cuentaRepository;
	@Mock BancoRepository bancoRepository;
	//@Autowired //implementacion de Spring, permite definir interfaz en lugar de impl concreta
	@InjectMocks CuentaServiceImp cuentaService;//implementacion de la intefaz para inyeccion de mocks

	@BeforeEach
	void setUp() {
		//reemplazando injeccion manual con anotaciones
//		cuentaRepository = mock(CuentaRepository.class);
//		bancoRepository = mock(BancoRepository.class);
//		cuentaService = new CuentaServiceImp(cuentaRepository, bancoRepository);
	}

	@Test
	void contextLoads() {
		when(cuentaRepository.findById(1L)).thenReturn(crearCuenta001());
		when(cuentaRepository.findById(2L)).thenReturn(crearCuenta002());
		when(bancoRepository.findById(1L)).thenReturn(crearBanco());
		//saldos
		BigDecimal saldoOrigen = cuentaService.consultarSaldo(1L);
		BigDecimal saldoDestino = cuentaService.consultarSaldo(2L);
		assertEquals("1000", saldoOrigen.toPlainString());
		assertEquals("2000", saldoDestino.toPlainString());
		//transferencia
		cuentaService.transferir(1L, 1L, 2L, new BigDecimal("100"));
		saldoOrigen = cuentaService.consultarSaldo(1L);
		saldoDestino = cuentaService.consultarSaldo(2L);
		assertEquals("900", saldoOrigen.toPlainString());
		assertEquals("2100", saldoDestino.toPlainString());
		int total = cuentaService.consultarTotalTransferencias(1L);
		assertEquals(1, total);
		//verify
		verify(cuentaRepository, times(3)).findById((1L));
		verify(cuentaRepository, times(3)).findById((2L));
		verify(cuentaRepository, times(2)).save(any(Cuenta.class));
		verify(bancoRepository, times(2)).findById(1L);
		verify(bancoRepository).save(any(Banco.class));

		verify(cuentaRepository, times(6)).findById((anyLong()));
		verify(cuentaRepository, never()).findAll();
	}

	@Test
	void contextLoads2() {
		when(cuentaRepository.findById(1L)).thenReturn(crearCuenta001());
		when(cuentaRepository.findById(2L)).thenReturn(crearCuenta002());
		when(bancoRepository.findById(1L)).thenReturn(crearBanco());
		//saldos
		BigDecimal saldoOrigen = cuentaService.consultarSaldo(1L);
		BigDecimal saldoDestino = cuentaService.consultarSaldo(2L);
		assertEquals("1000", saldoOrigen.toPlainString());
		assertEquals("2000", saldoDestino.toPlainString());
		//transferencia
		assertThrows(SaldoInsuficienteException.class, () -> {
			cuentaService.transferir(1L, 1L, 2L, new BigDecimal("1200"));
		});
		saldoOrigen = cuentaService.consultarSaldo(1L);
		saldoDestino = cuentaService.consultarSaldo(2L);
		assertEquals("1000", saldoOrigen.toPlainString());
		assertEquals("2000", saldoDestino.toPlainString());
		int total = cuentaService.consultarTotalTransferencias(1L);
		assertEquals(0, total);
		//verify
		verify(cuentaRepository, times(3)).findById((1L));
		verify(cuentaRepository, times(2)).findById((2L));
		verify(cuentaRepository, never()).save(any(Cuenta.class));
		verify(bancoRepository, times(1)).findById(1L);
		verify(bancoRepository, never()).save(any(Banco.class));

		verify(cuentaRepository, times(5)).findById((anyLong()));
		verify(cuentaRepository, never()).findAll();
	}

	@Test
	void compruebaMismaInstancia() {
		when(cuentaRepository.findById(1L)).thenReturn(crearCuenta001());
		Cuenta cta1 = cuentaService.findCuentaById(1L);
		Cuenta cta2 = cuentaService.findCuentaById(1L);
		assertSame(cta1, cta2);
		assertTrue(cta1 == cta2);
	}

	@Test
	void testFindAllCuentas() {
		List<Cuenta> datos = Arrays.asList(crearCuenta001().orElseThrow(), crearCuenta002().orElseThrow());
		when(cuentaRepository.findAll()).thenReturn(datos);
		List<Cuenta> cuentas = cuentaService.findAll();
		assertFalse(cuentas.isEmpty());
		assertEquals(2, cuentas.size());
		assertTrue(cuentas.contains(crearCuenta001().orElseThrow()));
		assertTrue(cuentas.contains(crearCuenta002().orElseThrow()));
		verify(cuentaRepository).findAll();
	}

	@Test
	void testSave() {
		Cuenta datoCuenta = new Cuenta(null, "Jaime Duende", new BigDecimal("1500"));
		when(cuentaRepository.save(any())).then(invocationOnMock -> {
			Cuenta c = invocationOnMock.getArgument(0);
			c.setIdCuenta(3L);
			return c;
		});
		Cuenta cuenta = cuentaService.save(datoCuenta);
		assertEquals("Jaime Duende", cuenta.getTitular());
		assertEquals(3, cuenta.getIdCuenta());
		assertEquals("1500", cuenta.getSaldo().toPlainString());
		verify(cuentaRepository).save(any());
	}
}
