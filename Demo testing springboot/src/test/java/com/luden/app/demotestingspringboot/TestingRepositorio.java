package com.luden.app.demotestingspringboot;

import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.repository.CuentaRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Tag("repositorio")
public class TestingRepositorio {
    @Autowired
    CuentaRepository cuentaRepository;

    @Test
    void findCuentaById() {
        Optional<Cuenta> cuenta = cuentaRepository.findById(1L);
        assertTrue(cuenta.isPresent());
        assertEquals("Carlos", cuenta.orElseThrow().getTitular());
        assertEquals(1L, cuenta.orElseThrow().getIdCuenta());
        assertEquals("1000.00", cuenta.orElseThrow().getSaldo().toPlainString());
    }

    @Test
    void findCuentaByTitular() {
        Optional<Cuenta> cuenta = cuentaRepository.findByTitular("Svein");
        assertTrue(cuenta.isPresent());
        assertEquals("Svein", cuenta.orElseThrow().getTitular());
        assertEquals(2L, cuenta.orElseThrow().getIdCuenta());
        assertEquals("2000.00", cuenta.orElseThrow().getSaldo().toPlainString());
    }

    @Test
    void findCuentaByTitularThrowException() {
        Optional<Cuenta> cuenta = cuentaRepository.findByTitular("NoExistePersona");
        assertThrows(NoSuchElementException.class, cuenta::orElseThrow);//segundo arg expresion lambda como metodo de referencia
        assertFalse(cuenta.isPresent());
    }

    @Test
    void findAll() {
        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertFalse(cuentas.isEmpty());
        assertEquals(3, cuentas.size());
    }

    @Test
    void saveCuenta() {
        //given
        Cuenta cuenta = new Cuenta(null ,"Jaime Duende", new BigDecimal("500"));
        //when
        cuentaRepository.save(cuenta);
        //then
        Cuenta jaimeDuendeCuenta = cuentaRepository.findByTitular("Jaime Duende").orElseThrow();
        assertEquals("Jaime Duende", jaimeDuendeCuenta.getTitular());
        //assertEquals(3L, jaimeDuendeCuenta.getIdCuenta());//apesar del rollback, bd gaurda secuencia de id's
    }

    @Test
    void saveCuentaConsultaIdNuevo() {
        //given
        Cuenta cuenta = new Cuenta(null ,"Jaime Duende", new BigDecimal("500"));
        //when
        Cuenta jaimeDuendeCuenta = cuentaRepository.save(cuenta);
        //then
        Cuenta  busquedaCuenta = cuentaRepository.findById(jaimeDuendeCuenta.getIdCuenta()).orElseThrow();
        assertEquals("Jaime Duende", busquedaCuenta.getTitular());
        //assertEquals(3L, jaimeDuendeCuenta.getIdCuenta());//apesar del rollback, bd gaurda secuencia de id's
    }

    @Test
    void modificarCuenta() {

        Cuenta cuenta = new Cuenta(null ,"Jaime Duende", new BigDecimal("500"));
        Cuenta jaimeDuendeCuenta = cuentaRepository.save(cuenta);

        Cuenta  busquedaCuenta = cuentaRepository.findById(jaimeDuendeCuenta.getIdCuenta()).orElseThrow();
        assertEquals("Jaime Duende", busquedaCuenta.getTitular());
        //assertEquals(3L, jaimeDuendeCuenta.getIdCuenta());//apesar del rollback, bd gaurda secuencia de id's
        busquedaCuenta.setSaldo(new BigDecimal("505"));
        Cuenta cuentaMod = cuentaRepository.save(busquedaCuenta);
        assertEquals("Jaime Duende", busquedaCuenta.getTitular());
        assertEquals("505", cuentaMod.getSaldo().toPlainString());
    }

    @Test
    void deleteCuenta() {
        Cuenta cuenta = cuentaRepository.findById(3L).orElseThrow();
        assertEquals("Otro", cuenta.getTitular());
        cuentaRepository.delete(cuenta);
        assertThrows(NoSuchElementException.class, () -> {
            cuentaRepository.findByTitular("Otro").orElseThrow();
        });
        assertEquals(2, cuentaRepository.findAll().size());
    }
}
