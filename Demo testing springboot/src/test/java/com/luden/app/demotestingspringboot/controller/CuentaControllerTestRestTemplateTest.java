package com.luden.app.demotestingspringboot.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.model.TransaccionDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.MediaType.*;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
//pruebas de integracion basadas en api Servlet

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Tag("integracion-rest-template-test")
public class CuentaControllerTestRestTemplateTest {

    @Autowired
    private TestRestTemplate testRestTemplate;
    ObjectMapper objectMapper;

    @LocalServerPort
    private int puerto;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    String crearUri(String uri){
        return "http://localhost:" + puerto + uri;
    }

    @Test
    @Order(1)
    void testTransferir() {//no requiere levantar app real, usa entorno de testing webEnviroment
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setMonto(new BigDecimal("100"));
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setBancoId(1L);

        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/cuenta/transferir", transaccionDto, String.class);
        String json = response.getBody();
        assertNotNull(json);// lo mismo que: assert json != null
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertTrue(json.contains("transferencia realizada con exito"));
    }

    @Test
    @Order(2)
    void testTransferirMostrarPuerto() throws JsonProcessingException {//no requiere levantar app real, usa entorno de testing webEnviroment
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setMonto(new BigDecimal("100"));
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setBancoId(1L);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("fecha", LocalDate.now().toString());
        responseMap.put("status", "ok");
        responseMap.put("mensaje", "transferencia realizada con exito");
        responseMap.put("detalle", transaccionDto);

        ResponseEntity<String> response = testRestTemplate.postForEntity(crearUri("/api/cuenta/transferir"), transaccionDto, String.class);
        System.out.println("Puerto en uso: " + puerto);
        String json = response.getBody();
        System.out.println("JSON respuesta: " + json);
        assertNotNull(json);// lo mismo que: assert json != null
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertTrue(json.contains("transferencia realizada con exito"));
        assertTrue(json.contains("{\"fecha\":\"2023-07-26\",\"mensaje\":\"transferencia realizada con exito\",\"status\":\"ok\",\"detalle\":{\"cuentaOrigen\":1,\"cuentaDestino\":2,\"monto\":100,\"bancoId\":1}}"));

        JsonNode jsonNode = objectMapper.readTree(json);
        assertEquals("transferencia realizada con exito", jsonNode.path("mensaje").asText());
        assertEquals(LocalDate.now().toString(), jsonNode.path("fecha").asText());
        assertEquals("100", jsonNode.path("detalle").path("monto").asText());
        assertEquals(1, jsonNode.path("detalle").path("cuentaOrigen").asLong());
        assertEquals(2, jsonNode.path("detalle").path("cuentaDestino").asLong());

        assertEquals(objectMapper.writeValueAsString(responseMap), json);
    }

    @Test
    @Order(3)
    void testDetalle() {
        ResponseEntity<Cuenta> response = testRestTemplate.getForEntity(crearUri("/api/cuenta/1"), Cuenta.class);
        Cuenta cuenta = response.getBody();

        assertNotNull(cuenta);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(1, cuenta.getIdCuenta());
        assertEquals("Carlos", cuenta.getTitular());
        assertEquals("800.00", cuenta.getSaldo().toPlainString());
        assertEquals(new Cuenta(1L, "Carlos", new BigDecimal("800.00")), cuenta);
    }

    @Test
    @Order(4)
    void testDetalleDos() {
        ResponseEntity<Cuenta> response = testRestTemplate.getForEntity(crearUri("/api/cuenta/2"), Cuenta.class);
        Cuenta cuenta = response.getBody();

        assertNotNull(cuenta);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(2, cuenta.getIdCuenta());
        assertEquals("Svein", cuenta.getTitular());
        assertEquals("2200.00", cuenta.getSaldo().toPlainString());
        assertEquals(new Cuenta(2L, "Svein", new BigDecimal("2200.00")), cuenta);
    }

    @Test
    @Order(5)
    void testListar() throws JsonProcessingException {
        ResponseEntity<Cuenta[]> response =  testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        List<Cuenta> cuentas = Arrays.asList(Objects.requireNonNull(response.getBody()));//evitando NullPonterEx para:
        //List<Cuenta> cuentas = Arrays.asList(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(3, cuentas.size());
        assertEquals(1, cuentas.get(0).getIdCuenta());
        assertEquals("Carlos", cuentas.get(0).getTitular());
        assertEquals("800.00", cuentas.get(0).getSaldo().toPlainString());
        assertEquals(2, cuentas.get(1).getIdCuenta());
        assertEquals("Svein", cuentas.get(1).getTitular());
        assertEquals("2200.00", cuentas.get(1).getSaldo().toPlainString());

        JsonNode jsonNode = objectMapper.readTree(objectMapper.writeValueAsString(cuentas));
        assertEquals(1L, jsonNode.get(0).path("idCuenta").asLong());
        assertEquals("Carlos", jsonNode.get(0).path("titular").asText());
        assertEquals("800.0", jsonNode.get(0).path("saldo").asText());
        assertEquals(2, jsonNode.get(1).path("idCuenta").asLong());
        assertEquals("Svein", jsonNode.get(1).path("titular").asText());
        assertEquals("2200.0", jsonNode.get(1).path("saldo").asText());
    }

    @Test
    @Order(6)
    void testGuardar() {
        Cuenta cuenta = new Cuenta(null, "NuevaCta", new BigDecimal("500"));
        ResponseEntity<Cuenta> response = testRestTemplate.postForEntity(crearUri("/api/cuenta"), cuenta, Cuenta.class);
        Cuenta nvaCta = response.getBody();
        assertNotNull(nvaCta);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(4, nvaCta.getIdCuenta());
        assertEquals("NuevaCta", nvaCta.getTitular());
        assertEquals("500", nvaCta.getSaldo().toPlainString());

        ResponseEntity<Cuenta[]> responseListado = testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        List<Cuenta> cuentas = Arrays.asList(Objects.requireNonNull(responseListado.getBody()));
        assertEquals(4, cuentas.size());
        assertEquals(new Cuenta(4L, "NuevaCta", new BigDecimal("500")), nvaCta);
    }

    @Test
    @Order(7)
    void testEliminar() {
        ResponseEntity<Cuenta[]> responseListado;
        List<Cuenta> cuentas;

        responseListado = testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        cuentas = Arrays.asList(Objects.requireNonNull(responseListado.getBody()));
        assertEquals(4, cuentas.size());

        testRestTemplate.delete(crearUri("/api/cuenta/4"));

        responseListado = testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        cuentas = Arrays.asList(Objects.requireNonNull(responseListado.getBody()));
        assertEquals(3, cuentas.size());

        ResponseEntity<Cuenta> response = testRestTemplate.getForEntity(crearUri("/api/cuenta/4"), Cuenta.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertFalse(response.hasBody());
    }

    @Test
    @Order(8)
    void testEliminarUsoExchange() {//exchange aplicado a todos los metodos HTTP
        ResponseEntity<Cuenta[]> responseListado;
        List<Cuenta> cuentas;

        responseListado = testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        cuentas = Arrays.asList(Objects.requireNonNull(responseListado.getBody()));
        assertEquals(3, cuentas.size());

        Map<String, Long> params = new HashMap<>();
        params.put("id", 3L);//mismo nombre del controller el path variable
        ResponseEntity<Void> exchange = testRestTemplate.exchange(crearUri("/api/cuenta/{id}"), HttpMethod.DELETE, null, Void.class, params);
        assertEquals(HttpStatus.NO_CONTENT, exchange.getStatusCode());
        assertFalse(exchange.hasBody());

        responseListado = testRestTemplate.getForEntity(crearUri("/api/cuenta"), Cuenta[].class);
        cuentas = Arrays.asList(Objects.requireNonNull(responseListado.getBody()));
        assertEquals(2, cuentas.size());

        ResponseEntity<Cuenta> response = testRestTemplate.getForEntity(crearUri("/api/cuenta/3"), Cuenta.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertFalse(response.hasBody());
    }

}
