package com.luden.app.demotestingspringboot.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.model.TransaccionDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

//pruebas de integracion basadas en Reactor, programacion reactiva
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)//ordenamermos metodos con anotaciones
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integracion-web-client-test")
class CuentaControllerWebTestClientTest {

    ObjectMapper objectMapper;

    @Autowired private WebTestClient webTestClient;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @Order(3)
    void testTransferir() throws JsonProcessingException {
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setBancoId(1L);
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setMonto(new BigDecimal("100"));
        Map<String, Object> resp = new HashMap();
        resp.put("fecha", LocalDate.now().toString());
        resp.put("status", "ok");
        resp.put("mensaje", "transferencia realizada con exito");
        resp.put("detalle", resp);
        webTestClient.post().uri("/api/cuenta/transferir")//mismo server cliente
        //webTestClient.post().uri("http://localhost:8080/api/cuenta/transferir")//cliente en diferente server
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(transaccionDto)
                        .exchange()//envia request, intercambio de request por respuesta: todo despues de este metodo
                        .expectStatus().isOk()
                        .expectBody()
                        .jsonPath("$.mensaje").isNotEmpty()
                        .jsonPath("$.mensaje").value(Matchers.is("transferencia realizada con exito"))
                        .jsonPath("$.mensaje").value(valor -> {
                            assertEquals("transferencia realizada con exito", valor);
                })
                .jsonPath("$.mensaje").isEqualTo("transferencia realizada con exito")
                .jsonPath("$.detalle.cuentaOrigen").isEqualTo(transaccionDto.getCuentaOrigen())
                .jsonPath("$.fecha").isEqualTo(LocalDate.now().toString());
                //.json(objectMapper.writeValueAsString(resp));//averiguar porque da stackoverflow exception
    }

    @Test
    @Order(4)
    void testTransferir2() throws JsonProcessingException {
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setBancoId(1L);
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setMonto(new BigDecimal("100"));
        Map<String, Object> resp = new HashMap();
        resp.put("fecha", LocalDate.now().toString());
        resp.put("status", "ok");
        resp.put("mensaje", "transferencia realizada con exito");
        resp.put("detalle", resp);
        webTestClient.post().uri("/api/cuenta/transferir")//mismo server cliente
                //webTestClient.post().uri("http://localhost:8080/api/cuenta/transferir")//cliente en diferente server
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(transaccionDto)
                .exchange()//envia request, intercambio de request por respuesta: todo despues de este metodo
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .consumeWith(respuesta -> {
                    try {
                        JsonNode json = objectMapper.readTree(respuesta.getResponseBody());
                        assertEquals("transferencia realizada con exito", json.path("mensaje").asText());
                        assertEquals(1L, json.path("detalle").path("cuentaOrigen").asLong());
                        assertEquals(LocalDate.now().toString(), json.path("fecha").asText());
                        assertEquals("100", json.path("detalle").path("monto").asText());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .jsonPath("$.mensaje").isNotEmpty()
                .jsonPath("$.mensaje").value(Matchers.is("transferencia realizada con exito"))
                .jsonPath("$.mensaje").value(valor -> {
                    assertEquals("transferencia realizada con exito", valor);
                })
                .jsonPath("$.mensaje").isEqualTo("transferencia realizada con exito")
                .jsonPath("$.detalle.cuentaOrigen").isEqualTo(transaccionDto.getCuentaOrigen())
                .jsonPath("$.fecha").isEqualTo(LocalDate.now().toString());
        //.json(objectMapper.writeValueAsString(resp));//averiguar porque da stackoverflow exception
    }

    @Test
    @Order(1)
    void testDetalleCuentaJsonPath() {
        webTestClient.get().uri("/api/cuenta/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.titular").isEqualTo("Carlos")
                .jsonPath("$.saldo").isEqualTo("1000.0");
    }

    @Test
    @Order(2)
    void testDetalleCuentaConsumeWith() {
        webTestClient.get().uri("/api/cuenta/2").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Cuenta.class)
                .consumeWith(cuentaEntityExchangeResult -> {
                    Cuenta cuenta =  cuentaEntityExchangeResult.getResponseBody();
                    assertNotNull(cuenta);
                    assertEquals("Svein", cuenta.getTitular());
                    assertEquals("2000.00", cuenta.getSaldo().toPlainString());
                });
    }

    @Test
    @Order(5)
    void testListarCuentas() {
        webTestClient.get().uri("/api/cuenta").exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isArray()
                .jsonPath("$").value(Matchers.hasSize(3))
                .jsonPath("$[0].titular").isEqualTo("Carlos")
                .jsonPath("$[0].idCuenta").isEqualTo("1")
                .jsonPath("$[0].saldo").isEqualTo("800.0")
                .jsonPath("$[1].titular").isEqualTo("Svein")
                .jsonPath("$[1].idCuenta").isEqualTo("2")
                .jsonPath("$[1].saldo").isEqualTo("2200.0");
    }
    @Test
    @Order(6)
    void testListarCuentas2() {
        webTestClient.get().uri("/api/cuenta").exchange()
                .expectStatus().isOk()
                .expectBodyList(Cuenta.class)
                .consumeWith(listEntityExchangeResult -> {
                    List<Cuenta> cuentas = listEntityExchangeResult.getResponseBody();
                    assertNotNull(cuentas);
                    assertFalse(cuentas.isEmpty());
                    assertEquals(3 ,cuentas.size());
                    assertEquals("Carlos", cuentas.get(0).getTitular());
                    assertEquals(1, cuentas.get(0).getIdCuenta());
                    assertEquals("800.0", cuentas.get(0).getSaldo().toPlainString());
                    assertEquals("Svein", cuentas.get(1).getTitular());
                    assertEquals(2, cuentas.get(1).getIdCuenta());
                    assertEquals("2200.0", cuentas.get(1).getSaldo().toPlainString());
                })
                .hasSize(3)
                .value(Matchers.hasSize(3));
    }

    @Test
    @Order(7)
    void testGuardarJsonPath() {
        Cuenta cuenta = new Cuenta(null, "Pedro Navajas", new BigDecimal("1500"));
        webTestClient.post().uri("/api/cuenta").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(cuenta)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()//default byte
                .jsonPath("$.titular").isEqualTo("Pedro Navajas")
                .jsonPath("$.titular").value(Matchers.is("Pedro Navajas"))
                .jsonPath("$.idCuenta").isEqualTo(4)
                .jsonPath("$.saldo").isEqualTo("1500");
    }

    @Test
    @Order(8)
    void testGuardarConsumeWith() {
        Cuenta cuenta = new Cuenta(null, "Pepito", new BigDecimal("500"));
        webTestClient.post().uri("/api/cuenta").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(cuenta)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Cuenta.class)
                .consumeWith(cuentaEntityExchangeResult -> {
                   Cuenta c = cuentaEntityExchangeResult.getResponseBody();
                   assertNotNull(c);
                   assertEquals("Pepito", c.getTitular());
                   assertEquals(5, c.getIdCuenta());
                   assertEquals("500", c.getSaldo().toPlainString());
                });
    }

    @Test
    @Order(9)
    void testEliminar() {
        webTestClient.get().uri("/api/cuenta").exchange().expectStatus().isOk().expectBodyList(Cuenta.class).hasSize(5);
        webTestClient.delete().uri("/api/cuenta/5")
                .exchange()
                .expectStatus().isNoContent()
                .expectBody().isEmpty();
        webTestClient.get().uri("/api/cuenta").exchange().expectStatus().isOk().expectBodyList(Cuenta.class).hasSize(4);
        webTestClient.get().uri("/api/cuenta/5").exchange().expectStatus()
                .isNotFound()
                .expectBody().isEmpty();

    }
}