package com.luden.app.demotestingspringboot.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.luden.app.demotestingspringboot.Datos;
import com.luden.app.demotestingspringboot.model.Cuenta;
import com.luden.app.demotestingspringboot.model.TransaccionDto;
import com.luden.app.demotestingspringboot.service.CuentaService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CuentaController.class)
@Tag("unitario-mvc")
class CuentaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CuentaService cuentaService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testListarCuentas() throws Exception {
        //given
        List<Cuenta> cuentas = Arrays.asList(Datos.crearCuenta001().orElseThrow(), Datos.crearCuenta002().orElseThrow());
        when(cuentaService.findAll()).thenReturn(cuentas);
        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/api/cuenta")
                .contentType(MediaType.APPLICATION_JSON))
                //then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].titular").value("Carlos"))
                .andExpect(jsonPath("$[1].titular").value("Svein"))
                .andExpect(jsonPath("$[0].saldo").value("1000"))
                .andExpect(jsonPath("$[1].saldo").value("2000"))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(cuentas)));
        verify(cuentaService).findAll();
    }

    @Test
    void testGuardar() throws Exception {
        Cuenta cuenta = new Cuenta(null, "Jaime Duende", new BigDecimal("1500"));
        when(cuentaService.save(any())).then(invocationOnMock -> {
           Cuenta c = invocationOnMock.getArgument(0);
           c.setIdCuenta(3L);
           return c;
        });
        mockMvc.perform(MockMvcRequestBuilders.post("/api/cuenta").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cuenta)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idCuenta").value(3))
                .andExpect(jsonPath("$.idCuenta", Matchers.is(3)))
                .andExpect(jsonPath("$.titular").value("Jaime Duende"))
                .andExpect(jsonPath("$.saldo").value("1500"));
        verify(cuentaService).save(any());
    }

    @Test
    void testDetalle() throws Exception {
        when(cuentaService.findCuentaById(1L)).thenReturn(Datos.crearCuenta001().orElseThrow());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/cuenta/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.titular").value("Carlos"))
                .andExpect(jsonPath("$.saldo").value("1000"));
        verify(cuentaService).findCuentaById(1L);
    }

    @Test
    void testTransferir() throws Exception {
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setMonto(new BigDecimal("100"));
        transaccionDto.setBancoId(1L);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/cuenta/transferir").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transaccionDto)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("$.fecha").value(LocalDate.now().toString()))
                        .andExpect(jsonPath("$.status").value("ok"))
                        .andExpect(jsonPath("$.mensaje").value("transferencia realizada con exito"))
                        .andExpect(jsonPath("$.detalle.cuentaOrigen").value(transaccionDto.getCuentaOrigen()))
                        .andExpect(jsonPath("$.detalle.cuentaDestino").value(transaccionDto.getCuentaDestino()));
        verify(cuentaService).transferir(1L, 1L, 2L, new BigDecimal("100"));
    }

    @Test
    void testTransferirComparaJsonCompleto() throws Exception {
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setCuentaOrigen(1L);
        transaccionDto.setCuentaDestino(2L);
        transaccionDto.setMonto(new BigDecimal("100"));
        transaccionDto.setBancoId(1L);
        System.out.println("JSON Request");
        System.out.println(objectMapper.writeValueAsString(transaccionDto));
        Map<String, Object> resp = new HashMap();
        resp.put("fecha", LocalDate.now().toString());
        resp.put("status", "ok");
        resp.put("mensaje", "transferencia realizada con exito");
        resp.put("detalle", transaccionDto);
        System.out.println("JSON Response");
        System.out.println(objectMapper.writeValueAsString(resp));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/cuenta/transferir")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaccionDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.fecha").value(LocalDate.now().toString()))
                .andExpect(jsonPath("$.status").value("ok"))
                .andExpect(jsonPath("$.mensaje").value("transferencia realizada con exito"))
                .andExpect(jsonPath("$.detalle.cuentaOrigen").value(transaccionDto.getCuentaOrigen()))
                .andExpect(jsonPath("$.detalle.cuentaDestino").value(transaccionDto.getCuentaDestino()))
                .andExpect(content().json(objectMapper.writeValueAsString(resp)));
        verify(cuentaService).transferir(1L, 1L, 2L, new BigDecimal("100"));
    }

}